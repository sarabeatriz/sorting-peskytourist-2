#include "mainwindow.h"
#include <iostream>
#include <cassert>
using namespace std;

void messageBox(const QString &st) {
    QMessageBox* msgBox     = new QMessageBox();
    msgBox->setWindowTitle("Pesky Tourist");
    msgBox->setText(st);
    msgBox->setWindowFlags(Qt::WindowStaysOnTopHint);
    msgBox->show();
}

// Given a vector of ints, sorts it in ascending order
void Sort(vector<int> &V){
    // Implement your selection sort algorithm here

}

// Given a (possibly unsorted) vector of ints, returns its median
int Median(vector<int> &V){

    // Implement your median function ...

    return 0;
}

// Given a (possibly unsorted) vector of QRgbs, returns the median
// composed of the medians of the color components.
QRgb MedianPixel(const vector<QRgb> &V){

    // Implement you pixel median function here....
    return qRgb(0,0,0);
}



void test_Sort() {
    
    // Implement your unit test for sort here .....

    // You may change this to a congratulatory message once you
    // implement this unit test :-)
    messageBox("Sort unit test not yet implemented!!");

}

void test_Median() {

    // Implement your unit test for median here .....

    messageBox("Median unit test not yet implemented!!");
}

void test_MedianPixel() {
    vector<QRgb> V;


    V.push_back(qRgb(10,20,30));
    V.push_back(qRgb(10,30,10));
    V.push_back(qRgb(20, 0,15));


    QRgb result = MedianPixel(V);

    assert(qRed(result) == 10);
    assert(qGreen(result) == 20);
    assert(qBlue(result) == 15);

    V.push_back(qRgb(0, 10,20));

    result = MedianPixel(V);
    assert(qRed(result) == 10);
    assert(qGreen(result) == 15);
    assert(qBlue(result) == 17);


    cout << "MedianPixel passed the test!!" << endl;
}




void MainWindow::RemoveNoise(const vector<QImage> & images, QImage & finalImage){

    // test_Sort();
    // test_Median();
    // test_MedianPixel();

    // Implement the filter here! See the algorithm in the lab
    messageBox("RemoveNoise function not yet implemented!!!");

}
