#include "dialog.h"
#include "ui_dialog.h"

#include <QMessageBox>
#include <QFileDialog>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);




    QDir myPath(QDir::homePath());
    myPath.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    myList = myPath.entryList();
    ui->listWidget->addItems(myList);
    ui->total_amount->setText(QString("%1").arg(ui->listWidget->count()));
}

Dialog::~Dialog()
{
    delete ui;
}

// Filter for folders
void Dialog::on_filter_textChanged(const QString &arg1)
{
    QRegExp regExp(arg1, Qt::CaseInsensitive, QRegExp::Wildcard);
    ui->listWidget->clear();
    ui->listWidget->addItems(myList.filter(regExp));
    ui->total_amount->setText(QString("%1").arg(ui->listWidget->count()));
}

// Open folder
void Dialog::on_pushButton_clicked()
{
   // QFileDialog::

    QMessageBox mbox;
    mbox.setWindowTitle("How to use the Simple Image Editor");
    QString test = ui->listWidget->currentItem()->text();
    if (ui->listWidget->currentItem() != NULL)
        mbox.setText(ui->listWidget->currentItem()->text());
    mbox.exec();
}

//this function is in charge of loading the
//parts of a creature in the given directory
void Dialog::loadItems(QString path, int N)
{
    QDir dir(path); //create a QDir with the given path

    dir.setFilter(QDir::Files);  //Set a filter so only files are looked for
    dir.setSorting(QDir::Name);  //Set sorting by name

    QFileInfoList list = dir.entryInfoList(); //get the list of files in the directory

    QFileInfo fileInfo;  //holds the info of the next file to be checked

    QString filePath; //string to store the path of the current file being checked
    QString fileName; //string to store the name of the current file being checked

    for(int i = 0; i < list.size(); i++)
    {
        fileInfo = list.at(i);  //get the file info at possition i
        filePath = fileInfo.filePath(); //get the file path
        fileName = fileInfo.fileName(); //get the file name

        //QImage image(filePath); //load the file as an image

        //if the file cant be loaded continue
        //if(image.isNull()) continue;

    }
}
