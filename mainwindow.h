#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QImage>

using namespace std;


//void RemoveNoise(QImage &, const vector<QImage> &) ;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow{
    Q_OBJECT

    public:

        /// \fn MainWindow::MainWindow(QWidget *parent)
        /// \~English
        /// \brief Constructor
        /// \~Spanish
        /// \brief Constructor
        explicit MainWindow(QWidget *parent = 0);


        /// \fn void MainWindow::RemoveNoise(QImage & finalImage, const vector<QImage> & images)
        /// \~English
        /// \brief Function that removes the noise from a set of images.
        /// For each position (i,j) of the pixels, insert the pixel in that position of each of
        /// the images in a pixels vector, sort the pixel vector, and finally set
        /// the median pixel to the new image in position (i,j)
        /// \param finalImage Image with the noise removed
        /// \param images Vector of images to remove the noise
        /// \~Spanish
        /// \brief Funcion que remueve el ruido de un conjunto de imagenes.
        /// Por cada posicion (i, j) de los pixeles, inserta en un vector el pixel en
        /// esa posicion por cada una de las imagenes, ordena el vector de pixeles, y finalmente
        /// ajusta el pixel (i,j) de la nueva imagen al pixel en la media.

        void RemoveNoise(const vector<QImage> & images, QImage & finalImage);

        /// \fn MainWindow::MainWindow(QWidget *parent)
        /// \~English
        /// \brief Destructor
        /// \~Spanish
        /// \brief Destructor
        ~MainWindow();

    private slots:

        /// \fn void MainWindow::on_btnLoadImages_clicked()
        /// \~English
        /// \brief Function that sets an image to a given label
        /// \~Spanish
        /// \brief Funcion que carga una imagen a una etiqueta del GUI.
        void on_btnLoadImages_clicked();

        /// \fn void MainWindow::on_btnRemoveNoise_clicked()
        /// \~English
        /// \brief Function that invokes the function that removes the noise from the images
        /// and set the returned image in the GUI
        /// \~Spanish
        /// \brief Funcion que invoca la funcion que remueve el ruido de las imagenes y
        /// ajusta la imagen que devuelve enn el GUI.
        void on_btnRemoveNoise_clicked();

        /// \fn  void MainWindow::on_btnSaveImage_clicked()
        /// \~English
        /// \brief Function that saves the image without noise in the computer.
        /// \~Spanish
        /// \brief Funcion que guarda la imagen sin ruido en la computadora.
        void on_btnSaveImage_clicked();

        /// \fn on_actLoad_Images_triggered()
        /// \~English
        /// \brief Call on_btnLoadImages_clicked() from the buttons
        /// \~Spanish
        /// \brief Invoca on_btnLoadImages_clicked() desde los botones
        void on_actLoad_Images_triggered();

        /// \fn on_actRemoveNoise_triggered()
        /// \~English
        /// \brief Call on_btnRemoveNoise_clicked() from the buttons
        /// \~Spanish
        /// \brief Invoca on_btnRemoveNoise_clicked() desde los botones
        void on_actRemoveNoise_triggered();

        /// \fn on_actSaveImage_triggered()()
        /// \~English
        /// \brief Call on_actSaveImage_triggered() from the buttons
        /// \~Spanish
        /// \brief Invoca on_actSaveImage_triggered() desde los botones
        void on_actSaveImage_triggered();

    private:
        Ui::MainWindow *ui;

        QImage finalImage;

        // Vectors that contain the image and its values
        vector<QImage> images;

        /// \fn void MainWindow::loadImages(vector<QImage> & images, QString path)
        /// \~English
        /// \brief Function that loads multiple images from a directory path
        /// \param images image vector for images with noise.
        /// \param path path of the directory with the images to load.
        /// \~Spanish
        /// \brief Funcion que carga multiples imagenes de un directorio
        /// \param images vector de imagenes con ruido
        /// \param path camino al directorio con las imagenes para cargar.
        void loadImages(vector<QImage> & , QString);
};

#endif // MAINWINDOW_H
